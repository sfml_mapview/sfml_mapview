#include "player.hpp"
#include "locals.hpp"
#include <iostream>
#include <string>


using std::cout;
using std::endl;


Player::Player( Scene * p ) : GameSprite( "ana.png" ) {
    
    parent = p;
    
    facing = SOUTH;
    step = 0;

    //~ setPosition( 20,20 );
    scale( 1,1 );
    speed = 185;
    vect = sf::Vector2f(0,0);
    
    setTextureRect( sf::IntRect( 0,0,width,height ) );
    width = SCALE;
    height = SCALE;
    facing = 0;
    moving = false;
    cycleTime = 650;
    
    layer = 0;
    
    drawOffset = sf::Vector2i(12,18);
}


Player::~Player() {

    parent = NULL;

}



int Player::getLayer() {
    
    return layer;
    
}




void Player::handle_input() {

    bool up     = sf::Keyboard::isKeyPressed(sf::Keyboard::W);
    bool down   = sf::Keyboard::isKeyPressed(sf::Keyboard::S);
    bool left   = sf::Keyboard::isKeyPressed(sf::Keyboard::A);
    bool right  = sf::Keyboard::isKeyPressed(sf::Keyboard::D);
    
    float xAxis = sf::Joystick::getAxisPosition(0,sf::Joystick::X);
    float yAxis = sf::Joystick::getAxisPosition(0,sf::Joystick::Y);

    if( sqrt(xAxis*xAxis + yAxis*yAxis) > 15 ) {
        vect.x = xAxis;
        vect.y = yAxis;
    }
    else {
        vect.x = right - left;
        vect.y = down - up;
    }
  
    if( vect != ZERO_VECT ) {
        
        moving = true;

        vect /= float( sqrt( vect.x*vect.x + vect.y*vect.y ) );
        float angle = atan2(vect.y,vect.x);
        
        // find quadrant
        if( angle > 0 ) {
            if( angle < 0.39f )
                facing = EAST;
            else if( angle < 1.18f )
                facing = SOUTHEAST;
            else if( angle < 1.97f )
                facing = SOUTH;
            else if( angle < 2.76f )
                facing = SOUTHWEST;
            else
                facing = WEST;
        }
        else {
            if( angle > -0.39f )
                facing = EAST;
            else if( angle > -1.18f )
                facing = NORTHEAST;
            else if( angle > -1.97f )
                facing = NORTH;
            else if( angle > -2.76f )
                facing = NORTHWEST;
            else
                facing = WEST;
        }
            
    }
    else
        moving = false;

}





void Player::update( int ticks ) {

    // check if a movement vector is requested
    if( vect != ZERO_VECT ) {

        // get total movement factor
        sf::Vector2f move_factor = vect*float(speed*ticks)/1000.0f;
            
        // try to move
        move(move_factor);

        // add ticks to movement time counter
        moveTime += ticks;
        
        // update current step
        int testTime = moveTime%cycleTime;
        if( testTime < cycleTime/4 )
            step = 0;
        else if( testTime < cycleTime/2 )
            step = 1;
        else if( testTime < 3*cycleTime/4 )
            step = 0;
        else
            step = 2;
    }
    else if( !moving ) {
        step = 0;
        moveTime = 0;
    }

    // set current image
    setTextureRect( sf::IntRect( step*width,facing*height,width,height ) );

}



void Player::clearVect() {
    
    vect = ZERO_VECT;
    
}



void Player::draw( sf::RenderTexture &window ) {
  
    window.draw( *this );

    window.display();

}



void Player::move( sf::Vector2f &someVect ) {

    sf::Vector2f xvect(someVect.x,0);
    sf::Vector2f yvect(0,someVect.y);

    // check collision
    if( !checkCollision(xvect) )
        gamePosition += xvect;
    if( !checkCollision(yvect) )
        gamePosition += yvect;



}


bool Player::checkCollision( sf::Vector2f t ) {
    
    bool ** map = parent->getCollisionMap(layer);
    int posx = round(t.x+gamePosition.x);
    int posy = round(t.y+gamePosition.y);
    
    posx /= SCALE;
    posy /= SCALE;
    
    return map[posy][posx];

}



void Player::setGamePosition(sf::Vector2f p) {
    
    gamePosition = p;
    
}



sf::Vector2i Player::getGamePosition() {
    
    return sf::Vector2i(round(gamePosition.x),round(gamePosition.y));

}


