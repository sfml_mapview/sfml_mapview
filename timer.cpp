#include "timer.hpp"
#include "funcs.hpp"


FrameTimer::FrameTimer() {

    current_fps = 0;
    tickCount = 0;
}

void FrameTimer::tick() {
  
    current = clock.getElapsedTime().asMicroseconds();
    ticks = current - lastTick;
  
    current_fps += 1000000.0f/ticks;
    tickCount++;
  
    lastTick = current;
}


float FrameTimer::getFPS() {

    if( tickCount >= 25 ) {

        avg_fps = current_fps/tickCount;
        tickCount = 0;
        current_fps = 0.0f;

    }
    return avg_fps;
}

int FrameTimer::getTicks() {
    return clock.getElapsedTime().asMilliseconds();
}
