#include "map.hpp"
#include "funcs.hpp"



Map::Map( std::string s, float sc ) {
    
    // parse the map
    std::string * map_text = get_map(s);

    map = new Tmx::Map;

    map->ParseText( *map_text );

    map_text = NULL;

    // find map stats
    width = map->GetWidth();	                // map size in tiles
    height = map->GetHeight();
    tileWidth = map->GetTileWidth();	        // tile size
    tileHeight = map->GetTileHeight();
    pxWidth = width * tileWidth;	            // map size in pixels
    pxHeight = height * tileHeight;
    Tmx::PropertySet mapProps = map->GetProperties();
    frames = mapProps.GetNumericProperty("frames");
    
    // find layer stats
    int layerCount = mapProps.GetNumericProperty("layers");
    
    for( int i = 0; i < layerCount; i++ ) {
        
        std::string currentLayer = "layer";
        currentLayer.push_back(i+48);
        int curLayerCount = mapProps.GetNumericProperty(currentLayer);
        
        int sum = 0;
        
        for( int j = 0; j < layerProps.size(); j++ )
            sum += layerProps[j].second;
        
        layerProps.push_back( std::pair<int,int>(sum,curLayerCount) );
        
    }

    // find tileset count
    set_count = map->GetNumTilesets();

    // allocate space for tileset pointers
    tileset = new sf::Sprite *[set_count];

    // iterate through tilesets
    for( int i = 0; i < set_count; i++ ) {
        
        // set pointer for each tileset
        tileset[i] = new sf::Sprite(*get_tileset(map
                            ->GetTileset(i)
                            ->GetName()+".png"));
    
    }

    // set position
    position = ZERO_VECT;

    // set zoom level
    float scale = float( SCALE / float( tileWidth ) );
    
    transform.scale( scale, scale );

    // allocate texture
    current.create( GAME_WIDTH, GAME_HEIGHT );
    current.clear( sf::Color(0, 0, 0, 0) );
    current.display();
    
    // set initial map time
    mapTime = 0;
    frameTime = 150;
    currentFrameTime = 0;

}


void Map::setVect( sf::Vector2f v ) {
    
    vect = v;
    
}


void Map::setPosition( int x, int y ) {
  
    position = sf::Vector2f( x, y );

}

void Map::setPosition( sf::Vector2f pos ) {
    
    position = pos;

}


Map::~Map() {
    
    for( int i = 0; i < set_count; i++ )
        tileset[i] = NULL;
    tileset = NULL;
        
    delete map;
    map = NULL;


}


void Map::update(int ticks) {
    
    mapTime += ticks;
    currentFrameTime += ticks;
    
    // handle rendering scenarios
    if( currentFrameTime > frameTime ) {
        
        if( vect != ZERO_VECT )
            move( float(ticks) * vect );
        
        //~ render();
        
        currentFrameTime %= frameTime;
    }
    else if( vect != ZERO_VECT ) {
        
        move( float(ticks) * vect );

        //~ render();
    }
}


void Map::draw(sf::RenderTexture & w) {
    
    //~ sf::Sprite temp( current.getTexture() );
    //~ 
    //~ w.draw(temp);
    //~ w.display();

}

//~ void Map::render() {
    //~ 
    //~ // init variables
    //~ int vertOffset,horizOffset;
    //~ int mapStartY,mapStartX;
    //~ int scrnStartY,scrnStartX;
    //~ int current_frame;
    //~ 
    //~ sf::Sprite * current_tileset;
    //~ sf::Vector2i pos = getPosition();
    //~ 
    //~ 
    //~ if( pos.y < 0 ) {
        //~ vertOffset = 0;
        //~ mapStartY = 0;
        //~ scrnStartY = abs(pos.y);
    //~ }
    //~ else {
        //~ vertOffset = pos.y % SCALE;
        //~ mapStartY = pos.y / SCALE;
        //~ scrnStartY = -vertOffset;
    //~ }
        //~ 
    //~ if( pos.x < 0 ) {
        //~ horizOffset = 0;
        //~ mapStartX = 0;
        //~ scrnStartX = abs(pos.x);
    //~ }
    //~ else {
        //~ horizOffset = pos.x % SCALE;
        //~ mapStartX = pos.x / SCALE;
        //~ scrnStartX = -horizOffset;
    //~ }
    //~ 
        //~ 
    //~ // clear current screen
    //~ current.clear( sf::Color(0, 0, 0, 0) );
//~ 
    //~ // iterate thru layers
    //~ for(int l = 0; l < map->GetNumLayers (); l++) {
//~ 
        //~ // get current layer
        //~ const Tmx::Layer * layer = map->GetLayer (l);
//~ 
        //~ // iterate thru rows
        //~ for(int i = 0; i - vertOffset < GAME_HEIGHT &&
                //~ mapStartY + i / SCALE < height; i += SCALE ) {
//~ 
            //~ // iterate thru cols
            //~ for(int j = 0; j - horizOffset < GAME_WIDTH &&
                        //~ mapStartX + j / SCALE < width; j += SCALE ) {
//~ 
                //~ Tmx::MapTile tile = layer->GetTile( mapStartX + j / SCALE,
                                                    //~ mapStartY + i / SCALE  );
//~ 
                //~ // if we have a legit tileset
                //~ if(tile.tilesetId != -1) {
                    //~ // get tile properties                    
                    //~ const Tmx::Tile * raw_tile = map
                                                //~ ->  GetTileset(tile.tilesetId)
                                                //~ ->  GetTile(tile.id);
                    //~ if( raw_tile != NULL ) {
                        //~ Tmx::PropertySet props = raw_tile->GetProperties();
                        //~ int tile_frames = props.GetNumericProperty("tile_frames");
                        //~ if( tile_frames != 0 )
                            //~ current_frame = (mapTime/frameTime) % tile_frames;
                        //~ else
                            //~ current_frame = 0;
                    //~ }
                    //~ else
                        //~ current_frame = 0;
//~ 
                    //~ // set a tileset pointer
                    //~ current_tileset = tileset[tile.tilesetId];
//~ 
                    //~ // get the tileset size
                    //~ sf::Vector2u size = current_tileset->getTexture()->getSize();
                    //~ int width = size.x;
                    //~ int height = size.y;
//~ 
                    //~ // find the rect for the tile
                    //~ sf::IntRect rect(((tile.id+current_frame)*tileWidth)%width,
                        //~ ((tile.id * tileWidth) / width) *
                            //~ tileHeight, tileWidth, tileHeight);
//~ 
                    //~ // set the tileset rect
                    //~ current_tileset -> setTextureRect(rect);
//~ 
                    //~ // set target position for the current tile
                    //~ int xpos = scrnStartX + j;
                    //~ int ypos = scrnStartY + i;
                    //~ current_tileset -> setPosition( xpos, ypos );
//~ 
                    //~ // render the tile
                    //~ current.draw(*current_tileset);
                    //~ 
                //~ }
            //~ }
        //~ }
    //~ }
    //~ 
    //~ current.display();
    //~ 
//~ }


void Map::renderLayerRow( int gameLayer, int gameRow, sf::RenderTexture & w ) {

    int startLayer = layerProps[gameLayer].first;
    int stopLayer = startLayer + layerProps[gameLayer].second;


    // init variables
    int vertOffset,horizOffset;
    int mapStartY,mapStartX;
    int scrnStartY,scrnStartX;
    int current_frame;
    
    sf::Sprite * current_tileset;
    sf::Vector2i pos = getPosition();
    
    
    if( pos.y < 0 ) {
        vertOffset = 0;
        mapStartY = 0;
        scrnStartY = abs(pos.y);
    }
    else {
        vertOffset = pos.y % SCALE;
        mapStartY = pos.y / SCALE;
        scrnStartY = -vertOffset;
    }
        
    if( pos.x < 0 ) {
        horizOffset = 0;
        mapStartX = 0;
        scrnStartX = abs(pos.x);
    }
    else {
        horizOffset = pos.x % SCALE;
        mapStartX = pos.x / SCALE;
        scrnStartX = -horizOffset;
    }
    
    // iterate thru layers
    for(int l = startLayer; l < stopLayer; l++) {

        // get current layer
        const Tmx::Layer * layer = map->GetLayer (l);
        
        // iterate thru cols
        for(int j = 0; j - horizOffset < GAME_WIDTH &&
                    mapStartX + j / SCALE < width; j += SCALE ) {

            Tmx::MapTile tile = layer->GetTile( mapStartX + j / SCALE,
                                                mapStartY + gameRow  );
                                                
            // if we have a legit tileset
            if(tile.tilesetId != -1) {
                // get tile properties                    
                const Tmx::Tile * raw_tile = map
                                            ->  GetTileset(tile.tilesetId)
                                            ->  GetTile(tile.id);
                if( raw_tile != NULL ) {
                    Tmx::PropertySet props = raw_tile->GetProperties();
                    int tile_frames = props.GetNumericProperty("tile_frames");
                    if( tile_frames != 0 )
                        current_frame = (mapTime/frameTime) % tile_frames;
                    else
                        current_frame = 0;
                }
                else
                    current_frame = 0;

                // set a tileset pointer
                current_tileset = tileset[tile.tilesetId];

                // get the tileset size
                sf::Vector2u size = current_tileset->getTexture()->getSize();
                int width = size.x;
                int height = size.y;

                // find the rect for the tile
                sf::IntRect rect(((tile.id+current_frame)*tileWidth)%width,
                    ((tile.id * tileWidth) / width) *
                        tileHeight, tileWidth, tileHeight);

                // set the tileset rect
                current_tileset -> setTextureRect(rect);

                // set target position for the current tile
                int xpos = scrnStartX + j;
                int ypos = scrnStartY + gameRow*SCALE;
                current_tileset -> setPosition( xpos, ypos );

                // render the tile
                w.draw(*current_tileset);
                
            }
        }        
    }
    
}



void Map::renderLayer( int gameLayer, sf::RenderTexture & w ) {

    int startLayer = layerProps[gameLayer].first;
    int stopLayer = startLayer + layerProps[gameLayer].second;


    // init variables
    int vertOffset,horizOffset;
    int mapStartY,mapStartX;
    int scrnStartY,scrnStartX;
    int current_frame;
    
    sf::Sprite * current_tileset;
    sf::Vector2i pos = getPosition();
    
    
    if( pos.y < 0 ) {
        vertOffset = 0;
        mapStartY = 0;
        scrnStartY = abs(pos.y);
    }
    else {
        vertOffset = pos.y % SCALE;
        mapStartY = pos.y / SCALE;
        scrnStartY = -vertOffset;
    }
        
    if( pos.x < 0 ) {
        horizOffset = 0;
        mapStartX = 0;
        scrnStartX = abs(pos.x);
    }
    else {
        horizOffset = pos.x % SCALE;
        mapStartX = pos.x / SCALE;
        scrnStartX = -horizOffset;
    }
    
        
    // clear current screen
    //~ w.clear( sf::Color(0, 0, 0, 0) );

    // iterate thru layers
    for(int l = startLayer; l < stopLayer; l++) {

        // get current layer
        const Tmx::Layer * layer = map->GetLayer (l);

        // iterate thru rows
        for(int i = 0; i - vertOffset < GAME_HEIGHT &&
                mapStartY + i / SCALE < height; i += SCALE ) {

            // iterate thru cols
            for(int j = 0; j - horizOffset < GAME_WIDTH &&
                        mapStartX + j / SCALE < width; j += SCALE ) {

                Tmx::MapTile tile = layer->GetTile( mapStartX + j / SCALE,
                                                    mapStartY + i / SCALE  );

                // if we have a legit tileset
                if(tile.tilesetId != -1) {
                    // get tile properties                    
                    const Tmx::Tile * raw_tile = map
                                                ->  GetTileset(tile.tilesetId)
                                                ->  GetTile(tile.id);
                    if( raw_tile != NULL ) {
                        Tmx::PropertySet props = raw_tile->GetProperties();
                        int tile_frames = props.GetNumericProperty("tile_frames");
                        if( tile_frames != 0 )
                            current_frame = (mapTime/frameTime) % tile_frames;
                        else
                            current_frame = 0;
                    }
                    else
                        current_frame = 0;

                    // set a tileset pointer
                    current_tileset = tileset[tile.tilesetId];

                    // get the tileset size
                    sf::Vector2u size = current_tileset->getTexture()->getSize();
                    int width = size.x;
                    int height = size.y;

                    // find the rect for the tile
                    sf::IntRect rect(((tile.id+current_frame)*tileWidth)%width,
                        ((tile.id * tileWidth) / width) *
                            tileHeight, tileWidth, tileHeight);

                    // set the tileset rect
                    current_tileset -> setTextureRect(rect);

                    // set target position for the current tile
                    int xpos = scrnStartX + j;
                    int ypos = scrnStartY + i;
                    current_tileset -> setPosition( xpos, ypos );

                    // render the tile
                    w.draw(*current_tileset);
                    
                }
            }
        }
    }

}


int Map::getLayerCount() {
    return layerProps.size();
}


void Map::move( sf::Vector2f v ) {
    position += v;
    vect = ZERO_VECT;
}

void Map::clearVect() {
    vect = ZERO_VECT;
}


sf::Vector2i Map::getPosition() {
    return sf::Vector2i( round(position.x), round(position.y) );
}

sf::Vector2i Map::getSize() {
    return sf::Vector2i( width,height );
}



bool ** Map::getCollisionMap(int gameLayer) {

    int startLayer = layerProps[gameLayer].first;
    int stopLayer = startLayer + layerProps[gameLayer].second;

    // creeate collision map
    bool ** collisionMap = new bool * [height];
    for(int i=0; i<height; i++) {
        collisionMap[i] = new bool [width];
    }

    // initialize collision map to 1s
    for( int i=0; i<height; i++ ) {
        for( int j=0; j<width; j++ ) {
            collisionMap[i][j] = 1;
        }
    }

    // iterate thru layers
    for(int l = startLayer; l < stopLayer; l++) {

        // get current layer
        const Tmx::Layer * layer = map->GetLayer (l);
        

        // iterate thru rows
        for(int i = 0; i < height; i++) {

            // iterate thru cols
            for(int j = 0; j < width; j++) {

                Tmx::MapTile tile = layer->GetTile( j,i );

                // if we have a legit tileset
                if(tile.tilesetId != -1) {
                    // get tile properties                    
                    const Tmx::Tile * raw_tile = map
                                                ->  GetTileset(tile.tilesetId)
                                                ->  GetTile(tile.id);
                    if( raw_tile != NULL ) {
                        int tile_col = raw_tile->
                            GetProperties().GetNumericProperty("tile_collision");
                        if( tile_col == 0 )
                            collisionMap[i][j] = false;
                    }                    
                }
            }
        }
        
    }
    return collisionMap;
}



void Map::printCollisionMap(int gameLayer) {
    
    int startLayer = layerProps[gameLayer].first;
    int stopLayer = startLayer + layerProps[gameLayer].second;

    // creeate collision map
    bool ** collisionMap = new bool * [height];
    for(int i=0; i<height; i++) {
        collisionMap[i] = new bool [width];
    }

    // initialize collision map to 1s
    for( int i=0; i<height; i++ ) {
        for( int j=0; j<width; j++ ) {
            collisionMap[i][j] = 1;
        }
    }

    // iterate thru layers
    for(int l = startLayer; l < stopLayer; l++) {

        // get current layer
        const Tmx::Layer * layer = map->GetLayer (l);
        

        // iterate thru rows
        for(int i = 0; i < height; i++) {

            // iterate thru cols
            for(int j = 0; j < width; j++) {

                Tmx::MapTile tile = layer->GetTile( j,i );

                // if we have a legit tileset
                if(tile.tilesetId != -1) {
                    // get tile properties                    
                    const Tmx::Tile * raw_tile = map
                                                ->  GetTileset(tile.tilesetId)
                                                ->  GetTile(tile.id);
                    if( raw_tile != NULL ) {
                        int tile_col = raw_tile->
                            GetProperties().GetNumericProperty("tile_collision");
                        if( tile_col == 0 )
                            collisionMap[i][j] = false;
                    }                    
                }
            }
        }        
    }
    
    // print result
    for(int i=0; i<height; i++) {
        for(int j=0; j<width; j++) {
            cout << collisionMap[i][j];
        }
        cout << endl;
    }
}


sf::Vector2i Map::getDimensions() {
    return sf::Vector2i(pxWidth,pxHeight);
}
