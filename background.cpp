#include "background.hpp"
#include "locals.hpp"



Background::Background() {

    background.create( GAME_WIDTH, GAME_HEIGHT );
    background.clear( sf::Color::Red );
    bg_sprite.setTexture( background.getTexture() );

}

Background::~Background() {
  
}

/*
void Background::update(int ticks) {

}

void Background::draw( sf::RenderWindow &window ) {

  window.draw( bg_sprite );

}*/
