#include <iostream>
#include <string>
#include "bg_stars.hpp"
#include "locals.hpp"
#include "funcs.hpp"


BgStars::BgStars( int dir, int num, float max, float spd ) {  
  // create initial bg
  background.create( GAME_WIDTH, GAME_HEIGHT );
  max_size = max;
  direction = dir;
  speed = spd;
  
  // initiate star list
  for(int i=0;i<num;i++)
    starlist.push_back( _make_Star(max_size) );

  switch( direction ) {
    case NORTH:
      dx = 0;
      dy = 1;
      break;
    case WEST:
      dx = -1;
      dy = 0;
      break;
    case SOUTH:
      dx = 0;
      dy = -1;
      break;
    case EAST:
      dx = 1;
      dy = 0;
      break;
  }
}


void BgStars::update( int ticks ) {
  sf::RectangleShape rect( sf::Vector2f(GAME_WIDTH,GAME_HEIGHT) );
  sf::RectangleShape star;

  // blank the bg
  rect.setFillColor( STARS_BG );
  background.draw( rect );

  // draw stars on the blank bg
  for(std::vector<Star>::iterator it=starlist.begin();it<starlist.end();it++) {

    // set circle parameters
    star.setPosition( it -> x_pos, it -> y_pos );
    star.setSize( sf::Vector2f( it->size,it->size ) );
    star.setFillColor( sf::Color(255,255,255) );

    // draw circle to bg
    background.draw( star );

    // advance star position
    (*it).x_pos += dx*( ticks*speed/1000.0*(*it).size/max_size );
    (*it).y_pos += dy*( ticks*speed/1000.0*(*it).size/max_size );

    // check for out of bounds
    if( !GAME_RECT.contains(it->x_pos,it->y_pos) ) {
      // respond to out of bounds based on starflow direction
      switch( direction ) {
        case NORTH:
          (*it).x_pos = rand()%GAME_WIDTH + 1;
          (*it).y_pos = 0;
          (*it).size = rand()%max_size + 1;
          break;
        case SOUTH:
          (*it).x_pos = rand()%GAME_WIDTH + 1;
          (*it).y_pos = GAME_HEIGHT - 1;
          (*it).size = rand()%max_size + 1;
          break;
        case EAST:
          (*it).x_pos = 0;
          (*it).y_pos = rand()%GAME_HEIGHT + 1;
          (*it).size = rand()%max_size + 1;
          break;
        case WEST:
          (*it).x_pos = GAME_WIDTH - 1;
          (*it).y_pos = rand()%GAME_HEIGHT + 1;
          int check = rand()%100+1;
          if( check > 25 ) (*it).size = 1;
          else (*it).size = 2;
          //(*it).size = rand()%max_size + 1;
          break;
            }
        }
    }
    bg_sprite.setTexture( background.getTexture() );
}


void BgStars::draw( sf::RenderTexture &window ) {

    window.draw( bg_sprite );

}


Star BgStars::_make_Star( int max_size ) {
    
    Star tempStar;

    tempStar.x_pos = rand()%GAME_WIDTH + 1;
    tempStar.y_pos = rand()%GAME_HEIGHT + 1;
    int check = rand()%100+1;
    
    if(check > 25) 
        tempStar.size = 1;
    else 
        tempStar.size = 2;

    return tempStar;

}

