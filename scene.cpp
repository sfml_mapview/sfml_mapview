#include <iostream>
#include <Thor/Particles.hpp>
#include <Thor/Math.hpp>
#include "funcs.hpp"
#include "scene.hpp"
#include "menu.hpp"




Scene::Scene() : GameState::GameState() {
    
    next = NULL;

    player = new Player( this );
    //~ player = NULL;

    // busy loop to prevent crash in font-loading
    while( !font.loadFromFile("cour.ttf") );

    scene_time = 0;

    bg = NULL;

    // load map
    map = new Map("simple.tmx");

    // get collision maps from map
    int layerCount = map->getLayerCount();
    for(int i=0; i<layerCount; ++i) {
        collisionMaps.push_back( map->getCollisionMap(i) );
        //~ map->printCollisionMap(i);
        //~ cout << endl; 
    }
    //~ cout << "gameplay layers: " << collisionMaps.size() << endl;

    // set player position
    sf::Vector2i size = map->getDimensions();
    while( player->checkCollision(ZERO_VECT) ) {
        int newx = rand()%size.x;
        int newy = rand()%size.y;
        player->setGamePosition(sf::Vector2f(newx,newy));
    }

    scrollSpeed = 1.25f;
    
    paused = false;

    // create pause text
    pauseText = sf::Text("Paused", font);
    pauseText.setStyle( sf::Text::Bold );
    sf::FloatRect bounds = pauseText.getLocalBounds();
    pauseText.setPosition(  GAME_WIDTH/2.f - bounds.width/2.f,
                            GAME_HEIGHT/2.f - bounds.height/2.f );

    // create status text
    statusText = sf::Text("", font, 15);
    //~ statusText.setStyle( sf::Text::Bold );
    statusText.setPosition( 15, GAME_HEIGHT-24 );

    // allocate texture
    current.create( GAME_WIDTH, GAME_HEIGHT );
    current.clear( sf::Color(0, 0, 0, 0) );
    current.display();
    
}



Scene::~Scene() {
  
    if( bg != NULL )
        delete bg;

    if( player != NULL )
        delete player;
        
    if( map != NULL )
        delete map;
}



void Scene::handle_events(sf::Event & event) {
    
    if( event.type == sf::Event::KeyPressed ) {
        if( event.key.code == sf::Keyboard::Escape ) {
            exit = 1;
            next = new Menu();
        }
        else if( event.key.code == sf::Keyboard::P ) {
            if( !paused ) pause();
            paused ^= 1;
        }
    }
    else if( event.type == sf::Event::LostFocus ) {
        pause();
        paused = true;
    }
     
}



void Scene::handle_input() {
  
    if( !paused ) {
        //~ // get joystick info
        //~ float x_axis = sf::Joystick::getAxisPosition(0,sf::Joystick::R);
        //~ float y_axis = sf::Joystick::getAxisPosition(0,sf::Joystick::Z);
        //~ 
        // get key info
        //~ bool up = sf::Keyboard::isKeyPressed(sf::Keyboard::Up);
        //~ bool down = sf::Keyboard::isKeyPressed(sf::Keyboard::Down);
        //~ bool left = sf::Keyboard::isKeyPressed(sf::Keyboard::Left);
        //~ bool right = sf::Keyboard::isKeyPressed(sf::Keyboard::Right);
        //~ bool shift = sf::Keyboard::isKeyPressed(sf::Keyboard::LShift);
        //~ bool pgup = sf::Keyboard::isKeyPressed(sf::Keyboard::PageUp);
        //~ bool pgdown = sf::Keyboard::isKeyPressed(sf::Keyboard::PageDown);
      
        //~ // set up movement vector
        //~ sf::Vector2i vect( right - left, down - up );
        //~ float mag = float(sqrt(x_axis*x_axis+y_axis*y_axis));
        //~ 
        //~ // scroll map
        //~ if( mag > 40 ) {
            //~ _scrollMapStick( sf::Vector2f(x_axis,y_axis), shift );
        //~ }
        //~ else if( vect != ZERO_VECT_I ) {
            //~ _scrollMapKeys( vect, shift );
        //~ }
        
        // handle player input
        if( player != NULL )
            player->handle_input();
    }
}




void Scene::update(int ticks) {

    if( !paused ) {

        scene_time += ticks;
        std::string status_string = "";
    
        if( bg != NULL )
            bg->update(ticks);

        if( player != NULL ) {
            
            player->update(ticks);
        
            auto pos = player->getGamePosition();
        
            map->setPosition(pos.x-GAME_WIDTH/2,pos.y-GAME_HEIGHT/2);
            
            status_string += ("Player: "+to_string(pos.x)+" "+to_string(pos.y));
        }
        
        if( map != NULL ) {
            
            map->update(ticks);
        
            auto pos = map->getPosition();
            
            status_string += (" Map: "+to_string(pos.x)+" "+to_string(pos.y));
            
        }
        
        statusText.setString( status_string );
        
        renderScene();
    }
    
    
    
}




void Scene::pause() {

        // clear movement vectors
        if( map != NULL )
            map -> clearVect();
        if( player != NULL )
            player -> clearVect();

}




void Scene::draw(sf::RenderTexture &window) {

    // draw background
    if( bg != NULL )
        bg -> draw( window );

    // draw map
    //~ if( map != NULL )
        //~ map -> draw( window );
        
    sf::Sprite temp( current.getTexture() );
    window.draw(temp);

    window.display();
    
    // draw sprites
    //~ if( player != NULL)
        //~ player -> draw( window );
    
    // draw pause text
    if( paused )
        window.draw( pauseText );
        
    // draw status text
    window.draw( statusText );
        
}


void Scene::renderScene() {
    
    // clear screen
    current.clear( sf::Color(0, 0, 0, 255) );
    
    // get map position/size
    auto pos = map->getPosition();
    auto size = map->getSize();
    size *= SCALE;
    int vertOffset = 0;
    
    // find vertical offset
    
    // find number of rows to draw
    int rows = SCREEN_ROWS;
    if( pos.y < 0 ) {
        rows -= abs(pos.y)/SCALE;
        vertOffset = 0;
    }
    else if( pos.y > size.y - GAME_HEIGHT ) {
        rows = (size.y - pos.y)/SCALE;
        vertOffset = size.y % SCALE;
    }
    else if( pos.y % SCALE != 0 ) {
        ++rows;
    }
    
    // render each layer in row-by-row fashion
    auto layerCount = map->getLayerCount();
    for(int i = 0; i < layerCount; ++i) {
    
        for(int r = 0; r < rows; ++r) {
            
            // render tile map
            map->renderLayerRow(i,r,current);
        
            // render map objects
            
            
            // render game sprites/objects
            
            
            // render player if necessary
            if( player != NULL && player->getLayer() == i ) {
            
                auto playerPos = player->getGamePosition();
                int relPosX = playerPos.x-pos.x;
                int relPosY = playerPos.y-pos.y;
                if( relPosX > 0 && relPosX < GAME_WIDTH &&
                    relPosY > -vertOffset && relPosY < r*SCALE-vertOffset ) {
                        
                    player->setPosition(relPosX-player->drawOffset.x,relPosY-player->drawOffset.y);
                    player->draw(current);
                
                }
            }
        }
    }
    
    // refresh display
    current.display();
    
}




void Scene::printCollisionMap(bool ** colMap) {
    
    if( map != NULL ) {
        sf::Vector2i size = map->getSize();
        
        // iterate over map rows and cols
        cout << endl;
        for(int i=0; i<size.y; i++) {
            for(int j=0; j<size.x; j++) {
                // print tile collision state
                cout << colMap[i][j];
            }
            cout << endl;
        }
    }
    
}

bool ** Scene::getCollisionMap(int layer) {
    return collisionMaps[layer];
}



