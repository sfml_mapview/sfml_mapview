#ifndef MAP_H
#define MAP_H

#include <SFML/Graphics.hpp>
#include "TmxParser/Tmx.h"


class Map {
  
  public:

    Map( std::string, float sc=1.0f );
    ~Map();
    void update( int );
    void draw( sf::RenderTexture& );
    
    //~ void render();
    void renderLayer(int,sf::RenderTexture&);
    void renderLayerRow(int,int,sf::RenderTexture&);
    
    void setVect(sf::Vector2f);
    void setPosition(int,int);
    void setPosition(sf::Vector2f);
    sf::Vector2i getPosition();
    sf::Vector2i getDimensions();
    bool ** getCollisionMap(int);
    void printCollisionMap(int);
    void move(sf::Vector2f);
    void clearVect();
    int getLayerCount();
    sf::Vector2i getSize();
  
  private:

    sf::RenderTexture current;
    Tmx::Map * map;
    sf::Sprite ** tileset;
    int set_count;
    std::vector<std::pair<int,int>> layerProps;
    
    // Map statistics
    int width;
    int height;
    int pxWidth;
    int pxHeight;
    int tileWidth;
    int tileHeight;
    int frames;
    
    // Position to render map (upper-left relative).
    sf::Vector2f position;
    sf::Vector2f vect;

    // Map rendering zoom level
    sf::Transform transform;
    
    // Map animation
    int mapTime;        // total time map has existed
    int frameTime;      // time per animation frame
    int currentFrameTime;
  
};


#endif // MAP_H
