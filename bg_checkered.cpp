#include "bg_checkered.hpp"
#include "locals.hpp"


BgCheckered::BgCheckered(sf::Color c1,sf::Color c2,int size) {
  
  background.create( SCRN_WIDTH, SCRN_HEIGHT );
  
  int x=0,y=0;
  sf::RectangleShape rect( sf::Vector2f(size,size) );
  
  sf::RenderTexture r1;
  sf::RenderTexture r2;
  
  r1.create( SCRN_WIDTH,size );
  r2.create( SCRN_WIDTH,size );
  
  sf::Sprite s1;
  sf::Sprite s2;
   
  while( x*size < SCRN_WIDTH ) {
    
    rect.setPosition( x*size,y );
    
    if( x%2 == 0 ) {
      
      rect.setFillColor( c1 );
      r1.draw( rect );
      rect.setFillColor( c2 );
      r2.draw( rect );

    }
    
    else {

      rect.setFillColor( c2 );
      r1.draw( rect );
      rect.setFillColor( c1 );
      r2.draw( rect );

    }
    
    x++;
  }
  
  s1.setTexture( r1.getTexture() );
  s2.setTexture( r2.getTexture() );
  
  x = 0;
  
  while( y*size < SCRN_HEIGHT ) {
    
    if( y%2 == 0 ) {
    
      s1.setPosition( 0, y*size );  
      background.draw( s1 );
      
    }
    
    else {
      
      s2.setPosition( 0, y*size );
      background.draw( s2 );
      
    }

    y++;
  }

  bg_sprite.setTexture( background.getTexture() );

}

void BgCheckered::update(int) {

}

void BgCheckered::draw(sf::RenderWindow &window) {
  window.draw( bg_sprite );
}
