#include "bg_solid.hpp"
#include "locals.hpp"

BgSolid::BgSolid() {
  bg_text.create( GAME_WIDTH, GAME_HEIGHT );
  bg_text.clear( sf::Color( 128,128,128 ) );
  background.setTexture( bg_text.getTexture() );
}

BgSolid::BgSolid( sf::Color c ) {  
  bg_text.create( GAME_WIDTH, GAME_HEIGHT );
  bg_text.clear( c );
  background.setTexture( bg_text.getTexture() );
}

void BgSolid::update(int ticks) {
  // nothing to do
}


void BgSolid::draw(sf::RenderTexture &window) {
  window.draw( background );
}
