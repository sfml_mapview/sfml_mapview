#ifndef FUNCS_H
#define FUNCS_H

#include <iostream>
    using std::cout;
    using std::endl;
#include <string>
#include <sstream>
#include <dirent.h>
#include <math.h>
#include <SFML/Graphics.hpp>
#include "TmxParser/TmxMap.h"

#include "locals.hpp"

template<class T>
inline std::string to_string( const T &t ) {
    std::stringstream ss;
    ss << t;
    return ss.str();
}


inline sf::Texture * get_sprite( std::string s ) {

    sf::Texture * tmpText;
  
    if( SpriteLib[s] != NULL )
        tmpText = SpriteLib[s];
    
    else {
        tmpText = new sf::Texture();
        tmpText->loadFromFile(SPRITE+s);
        SpriteLib[s] = tmpText;
    }
  
    return tmpText;

}

inline void load_sprites() {
    DIR * pdir = NULL;
    struct dirent * pent = NULL;

    pdir = opendir( SPRITE.c_str() );

    while( pent = readdir( pdir ) ) {

        if( (pent->d_name)[0] == '.' ) {
        }
        else {
          get_sprite( pent->d_name );
        }

    }

    closedir(pdir);
}

inline sf::Texture * get_bg( std::string s ) {
    sf::Texture * tmpText;

    if( BgLib[s] != NULL )
        tmpText = BgLib[s];

    else {
        tmpText = new sf::Texture();
        tmpText->loadFromFile(BG+s);
        BgLib[s] = tmpText;
    }

    return tmpText;
}


inline void load_bg() {
    DIR * pdir = NULL;
    struct dirent * pent = NULL;

    pdir = opendir( BG.c_str() );

    while( pent = readdir( pdir ) ) {
        if( (pent->d_name)[0] == '.' ) {
        }
        else {
            get_bg( pent->d_name );
        }
    }

    closedir(pdir);
}


inline std::string * get_map( std::string fileName ) {

    std::string * text;
    char* fileText;
    int fileSize;

    if( MapLib[fileName] != NULL )
        text = MapLib[fileName];
    
    else {
        
      // Open the file for reading.
      FILE *file = fopen( (MAP+fileName).c_str(), "rb" );

      // Find out the file size.
      fseek(file, 0, SEEK_END);
      fileSize = ftell(file);
      fseek(file, 0, SEEK_SET);

      // Allocate memory for the file and read it into the memory.
      fileText = new char[fileSize];
      fread(fileText, 1, fileSize, file);
  
      fclose(file);
  
      // Copy the contents into a C++ string and delete it from memory.
      text = new std::string(fileText, fileText+fileSize);
      delete [] fileText;
      
      MapLib[fileName] = text;
    }
    
    return text;
}



inline void load_map() {
  DIR * pdir = NULL;
  struct dirent * pent = NULL;
  
  pdir = opendir( MAP.c_str() );
  
  while( pent = readdir( pdir ) ) {
    if( (pent->d_name)[0] == '.' ) {
    }
    else {
      get_map( pent->d_name );
    }
  }
  
  closedir(pdir);
}


inline sf::Texture * get_tileset( std::string s ) {
    sf::Texture * tmpText;

    if( TilesetLib[s] != NULL )
        tmpText = TilesetLib[s];

    else {
        tmpText = new sf::Texture();
        tmpText->loadFromFile(TILESET+s);
        TilesetLib[s] = tmpText;
    }

    return tmpText;
}


inline void load_tileset() {
    DIR * pdir = NULL;
    struct dirent * pent = NULL;

    pdir = opendir( TILESET.c_str() );

    while( pent = readdir( pdir ) ) {
        if( (pent->d_name)[0] == '.' ) {
        }
        else {
            get_tileset( pent->d_name );
        }
    }
      
    closedir(pdir);
}


inline float round(float r) {
    return floor(r + 0.5f);
}


inline bool lessVect(sf::Vector2f & a,sf::Vector2f & b) {
    
    if( a.y < b.y )
        return true;
    
    else if( a.y == b.y )
        return a.x < b.x;
    
    else
        return false;
        
}


inline void recreateWindow( sf::RenderWindow &window ) {

    if( !FULLSCREEN )
        window.create(sf::VideoMode(SCRN_WIDTH,SCRN_HEIGHT),TITLE,
                                    sf::Style::Close|sf::Style::Resize,
                                    CONTEXT);
    
    else if( sf::VideoMode(SCRN_WIDTH,SCRN_HEIGHT).isValid() )
        window.create(sf::VideoMode(SCRN_WIDTH,SCRN_HEIGHT),TITLE,
                                sf::Style::Close|sf::Style::Fullscreen,
                                CONTEXT);
    
    else
        window.create(sf::VideoMode::getDesktopMode(),TITLE,
                                sf::Style::Close|sf::Style::Fullscreen,
                                CONTEXT);

    window.setMouseCursorVisible(false);
    window.setVerticalSyncEnabled(VSYNC);   

}


inline void print( void * input ) {
}




#endif
