#ifndef BG_STARS_H
#define BG_STARS_H

#include <vector>
#include "background.hpp"
#include "locals.hpp"



struct Star {
  
    float x_pos;
    float y_pos;
    int size;
  
};


class BgStars : public Background {
  
    public:
        BgStars(int dir=WEST,int num=75,float size=2,float spd=45);
        void update(int);
        void draw(sf::RenderTexture&);
        Star _make_Star(int);
    
    private:
        sf::RenderTexture background;
        sf::Sprite bg_sprite;
        std::vector<Star> starlist;
        int max_size,dx,dy,direction,speed;
    
};



#endif
