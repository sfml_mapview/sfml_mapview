#include <SFML/System/Clock.hpp>
#include <deque>

#ifndef FRAMETIMER_HPP
#define FRAMETIMER_HPP



class FrameTimer {

    public:
        FrameTimer();
        // ~FrameTimer();
        void tick();
        float getFPS();
        int getTicks();
  
    private:
        std::deque<float> fpsreg;
        int lastTick,current,ticks,tickCount;
        float current_fps;
        float avg_fps;
        sf::Clock clock;
        
};




#endif
