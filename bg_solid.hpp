#ifndef BG_SOLID_H
#define BG_SOLID_H

#include "background.hpp"



class BgSolid : public Background {
  
    public:
        BgSolid();
        BgSolid( sf::Color );
        void update(int);
        void draw(sf::RenderTexture&);
    
    private:
        sf::RenderTexture bg_text;
        sf::Sprite background;
        sf::Color color;

};



#endif
