#include "gamesprite.hpp"
#include "funcs.hpp"


GameSprite::GameSprite( const std::string &texture_str )
  : sf::Sprite( *get_sprite(texture_str) ) {
  
  alive = true;
  
}

//~ GameSprite::~GameSprite() {}
//~ 
//~ void GameSprite::handle_input(sf::Event){}
//~ void GameSprite::update(int){}
//~ void GameSprite::draw(sf::RenderWindow&){}
