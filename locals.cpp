#include "locals.hpp"
#include "funcs.hpp"

std::map<std::string,sf::Texture *> SpriteLib;
std::map<std::string,sf::Texture *> BgLib;
std::map<std::string,std::string *> MapLib;
std::map<std::string,sf::Texture *> TilesetLib;

const std::string SPRITE        = "sprite/";
const std::string BG            = "bg/";
const std::string MAP           = "map/";
const std::string TILESET       = "tileset/";

// int type maximums
const int MAX_UINT8             = 255;
const std::string TITLE         = "SFML Mapview";
const bool DEBUG                = 0;

// screen size 
const int GAME_WIDTH            = 640;
const int GAME_HEIGHT           = 360;
const int SCRN_WIDTH            = 1280;
const int SCRN_HEIGHT           = 720;
float SCALEX                    = float(SCRN_WIDTH)/float(GAME_WIDTH);
float SCALEY                    = float(SCRN_HEIGHT)/float(GAME_HEIGHT);
bool FULLSCREEN                 = 0;
bool VSYNC                      = 0;
const sf::IntRect SCRN_RECT     ( sf::Vector2i(0,0),
                                  sf::Vector2i(SCRN_WIDTH,SCRN_HEIGHT) );
const sf::IntRect GAME_RECT     ( sf::Vector2i(0,0),
                                  sf::Vector2i(GAME_WIDTH,GAME_HEIGHT) );
const int SCALE                 = 24;
const int SCREEN_ROWS           = GAME_HEIGHT/SCALE;  // the # of map rows to draw on a single screen

const sf::ContextSettings CONTEXT = sf::ContextSettings(0,0,0,3,0);

// game loop stuff
const int FPS                   = 60;
const float SKIP_FRAMES         = 1000.0f/FPS;
const int SKIP_FRAMES_MAX       = 5;


const sf::Color BLACK           (0,0,0);
const sf::Color WHITE           (255,255,255);
const sf::Color STARS_BG        (12,12,30);

const char SPACE                = ' ';
const char NEWLINE              = '\n';
const sf::Vector2i ZERO_VECT_I  = sf::Vector2i(0,0);
const sf::Vector2f ZERO_VECT    = sf::Vector2f(0.0f,0.0f);
const int ZERO                  = 0;

// star stuff
const int STARS_DIST            = round(SCRN_HEIGHT*(64/600.0f));
const int STARS_MED             = round(SCRN_HEIGHT*(34/600.0f));
const int STARS_NEAR            = round(SCRN_HEIGHT*(17/600.0f));
