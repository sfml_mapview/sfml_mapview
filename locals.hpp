#ifndef LOCALS_H
#define LOCALS_H

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <map>
#include <string>


// **** global constants ****


extern std::map<std::string,sf::Texture *> SpriteLib;
extern std::map<std::string,sf::Texture *> BgLib;
extern std::map<std::string,std::string *> MapLib;
extern std::map<std::string,sf::Texture *> TilesetLib;

extern const std::string SPRITE;
extern const std::string BG;
extern const std::string MAP;
extern const std::string TILESET;


// int type maximums
extern const int MAX_UINT8;
extern const std::string TITLE;
extern const bool DEBUG;

// screen size
extern const int GAME_WIDTH;
extern const int GAME_HEIGHT;
extern const int SCRN_WIDTH;
extern const int SCRN_HEIGHT;
extern float SCALEX;
extern float SCALEY;
extern bool FULLSCREEN;
extern bool VSYNC;
extern const sf::IntRect SCRN_RECT;
extern const sf::IntRect GAME_RECT;
extern const int SCALE;
extern const int SCREEN_ROWS;

extern const sf::ContextSettings CONTEXT;

// game loop stuff
extern const int FPS;
extern const float SKIP_FRAMES;
extern const int SKIP_FRAMES_MAX;

//~ enum Directions {
  //~ NORTH, WEST, SOUTH, EAST
//~ };


enum dirs {
    SOUTH,WEST,EAST,NORTH,
    SOUTHWEST,SOUTHEAST,
    NORTHWEST,NORTHEAST
};

enum DIRS {
    DOWN,LEFT,RIGHT,UP
};

extern const sf::Color BLACK;
extern const sf::Color WHITE;
extern const sf::Color STARS_BG;

extern const char SPACE;
extern const char NEWLINE;
extern const sf::Vector2i ZERO_VECT_I;
extern const sf::Vector2f ZERO_VECT;
extern const int ZERO;

// star stuff
extern const int STARS_DIST;
extern const int STARS_MED;
extern const int STARS_NEAR;


#endif
