OBJECTS = locals.o timer.o gamestate.o background.o bg_solid.o bg_checkered.o bg_stars.o bg_scroll.o map.o menu.o scene.o gamesprite.o player.o ballistic.o
SOURCES = $(OBJECTS:.o=.cpp)
SRCDIR  = Sources 

TMXSRCS = $(shell ls TmxParser/*.cpp)
TMXOBJS = $(TMXSRCS:.cpp=.o)
TMXDIR  = TmxParser

BASE64SRCS = $(shell ls TmxParser/base64/*.cpp)
BASE64OBJS = $(BASE64SRCS:.cpp=.o)
BASE64DIR = TmxParser/base64

TIXMLSRCS = $(shell ls TmxParser/tinyxml/*.cpp)
TIXMLOBJS = $(TIXMLSRCS:.cpp=.o)
TIXMLDIR  = TmxParser/tinyxml


LIBS	= -lsfml-graphics -lsfml-audio -lsfml-window -lsfml-system -lthor -lz -lglut -lGLEW -lGL
#CFLAGS 	= -g -Wno-deprecated -std=c++11 -O3
CFLAGS = -g -Wno-deprecated -std=c++11 
CC		= g++
EXECUTABLE = main.out



main: $(TIXMLOBJS) $(BASE64OBJS) $(TMXOBJS) $(OBJECTS) driver.cpp
		g++ -o $(EXECUTABLE) $(CFLAGS) $(TIXMLOBJS) $(BASE64OBJS) $(TMXOBJS) $(OBJECTS) driver.cpp $(LIBS)

ballistic.o: ballistic.hpp ballistic.cpp
		g++ -c ballistic.cpp $(CFLAGS)
player.o: player.hpp player.cpp
		g++ -c player.cpp $(CFLAGS)
gamesprite.o: gamesprite.hpp gamesprite.cpp
		g++ -c gamesprite.cpp $(CFLAGS)
scene.o: scene.hpp scene.cpp
		g++ -c scene.cpp $(CFLAGS)
menu.o: menu.hpp menu.cpp
		g++ -c menu.cpp $(CFLAGS)
map.o: map.hpp map.cpp
		g++ -c map.cpp $(CFLAGS)
bg_scroll.o: bg_scroll.hpp bg_scroll.cpp
		g++ -c bg_scroll.cpp $(CFLAGS)
bg_stars.o: bg_stars.hpp bg_stars.cpp
		g++ -c bg_stars.cpp $(CFLAGS)
bg_checkered.o: bg_checkered.hpp bg_checkered.cpp
		g++ -c bg_checkered.cpp $(CFLAGS)
bg_solid.o: bg_solid.hpp bg_solid.cpp
		g++ -c bg_solid.cpp $(CFLAGS)
background.o: background.hpp background.cpp
		g++ -c background.cpp $(CFLAGS)
gamestate.o: gamestate.hpp gamestate.cpp
		g++ -c gamestate.cpp $(CFLAGS)
timer.o: timer.hpp timer.cpp
		g++ -c timer.cpp $(CFLAGS)
locals.o: locals.hpp locals.cpp
		g++ -c locals.cpp $(CFLAGS)





clean:
		rm main.out; rm *.o; rm $(BASE64DIR)/*.o; rm $(TMXDIR)/*.o; rm $(TIXMLDIR)/*.o
