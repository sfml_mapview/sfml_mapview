#ifndef BG_CHECKERED_H
#define BG_CHECKERED_H

#include "background.hpp"



class BgCheckered : public Background {

    public:
        BgCheckered(  sf::Color c1 = sf::Color(180,180,180),
                      sf::Color c2 = sf::Color(150,150,150),
                      int size = 250                          );
        void update(int);
        void draw(sf::RenderWindow&);
    
  
    private:
        sf::RenderTexture background;
        sf::Sprite bg_sprite;
    
};






#endif
