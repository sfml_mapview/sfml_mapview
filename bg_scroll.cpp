#include "bg_scroll.hpp"
#include "funcs.hpp"

typedef std::vector<Layer>::iterator vlayer_it;

// implicit (do-nothing) constructor
Layer::Layer() {}

// default constructor (use this!)
Layer::Layer( sf::Texture * txt, int dir, int xpos, int ypos, int spd ) {
  
  // set layer attributes
  xPos = xpos;
  yPos = ypos;
  
  direction = dir;
  speed = spd;
  
  img.setTexture( *txt );
  
  sf::Vector2u vect = img.getTexture() -> getSize();
  
  width = vect.x;
  height = vect.y;
  
  shift_pos = 0.0f;


  if( direction % 2 == 0 )
    rect = sf::IntRect( 0,0,width,SCRN_HEIGHT );
  else
    rect = sf::IntRect( 0,0,SCRN_WIDTH,height );
  
  img.setTextureRect( rect );

  img.setPosition( xPos,yPos );
  
}




void Layer::shift(const int &time) {

  float factor = (time/1000.0f)*speed;
  shift_pos += factor;

  // if scrolling vertically
  if( direction%2 == 0 ) {
    
  }
  // if scrolling horizontally
  else {
    rect.left = int(round(shift_pos))%(width-SCRN_WIDTH);
  }
  
  img.setTextureRect( rect );
  
}





BgScroll::BgScroll() {
  
  speed = 1.0f;
  
  layers.push_back(Layer(get_bg("space.png"),WEST,0,0,4 ) );
  layers.push_back(Layer(get_bg("mountains.png"),WEST,0,SCRN_HEIGHT-227,16 ) );
  layers.push_back(Layer(get_bg("dirt.png"),WEST,0,SCRN_HEIGHT-63,32 ) );
  
}


BgScroll::BgScroll( const std::vector<Layer> &l, float spd ) {
  
  // set data members
  speed = spd;
  layers = l;
  
  // create initial background image
  for(vlayer_it it=layers.begin();it!=layers.end();it++) {
    if( (it->direction)%2 == 0 )
      (it->img).setTextureRect( sf::IntRect( 0,0,it->width,SCRN_HEIGHT ) );
    else
      (it->img).setTextureRect( sf::IntRect( 0,0,SCRN_WIDTH,it->height ) );
  }
  
}


//BgScroll::~BgScroll() {}      // no need?


void BgScroll::update( int ticks ) {
  
  for(std::vector<Layer>::iterator it=layers.begin();it!=layers.end();it++) {
    
    it -> shift( ticks*speed );
  
  }
  
}


void BgScroll::draw( sf::RenderWindow &window ) {
  
  for(std::vector<Layer>::iterator it=layers.begin();it!=layers.end();it++) {
    window.draw( it->img );
  }
  
}

