#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <SFML/Graphics.hpp>


class Background {
  
    public:
        Background();
        virtual ~Background();
        virtual void update(int)=0;
        virtual void draw(sf::RenderTexture&)=0;
    
  
    private:
        sf::RenderTexture background;
        sf::Sprite bg_sprite;
  
};




#endif