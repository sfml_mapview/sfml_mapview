#include <string>
#include <math.h>

// provisional for testing
#include <GL/glew.h> // glew must be included before the main gl libs
#include <GL/glut.h> // doing otherwise causes compiler shouting


#include <glm/glm.hpp>
#define GLM_FORCE_RADIANS
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp> //Makes passing matrices to shaders easier

#include "funcs.hpp"
#include "timer.hpp"
#include "gamestate.hpp"
#include "gamesprite.hpp"
#include "menu.hpp"
#include "locals.hpp"


using std::cout;
using std::endl;


int main() {
    // initiate random seed
    std::srand(time(0));

    // preload sound and image data
    load_sprites();
    load_bg();
    load_map();
    load_tileset();

    // Create the main window
    sf::RenderWindow window;
    window.create(sf::VideoMode(SCRN_WIDTH,SCRN_HEIGHT),TITLE,
                                            sf::Style::Close,
                                            CONTEXT);
    sf::RenderTexture texture;
    texture.create(GAME_WIDTH,GAME_HEIGHT);
    
    // check screen size
    sf::Vector2u size = window.getSize();
    if( float(size.x)/size.y != float(GAME_WIDTH)/GAME_HEIGHT )
        texture.setSmooth(true);
    else
        texture.setSmooth(false);
    
    // Create misc stuff here
    sf::ContextSettings cs = window.getSettings();
    cout << cs.majorVersion << cs.minorVersion << endl;
    

    // declare variables
    bool show_fps = 0;
    std::string fps_str;
    FrameTimer clock;
    //sf::Clock game_time;
    int ticks,update_ticks=0,last_update=0,loops;
    float lastframe;
    sf::Event event;
    GameState * game_state = new Menu();

    // Create a graphical text to display
    sf::Font font;
    if (!font.loadFromFile("cour.ttf")) return EXIT_FAILURE;
    sf::Text text("", font, 18);
    text.setStyle(sf::Text::Bold);


    recreateWindow(window);
    lastframe = float(clock.getTicks());

  
    // Start the game lo98rop
    while( window.isOpen() ) {
        // update current time
        clock.tick();

        // Process events
        while ( window.pollEvent(event) ) {
            // Close window : exit
            if ( event.type == sf::Event::KeyPressed ) {

                if( event.key.code == sf::Keyboard::F6 ) // F6
                    show_fps ^= 1;      // flip show_fps flag
                    
                else if( event.key.code == sf::Keyboard::F11 ) { // F11
                    // toggle fullscreen mode
                    FULLSCREEN ^= 1;
                    recreateWindow( window );
                    
                    // check screen size:game size ratio
                    sf::Vector2u size = window.getSize();
                    if( float(size.x)/size.y != float(GAME_WIDTH)/GAME_HEIGHT )
                        texture.setSmooth(true);
                    else
                        texture.setSmooth(false);
                        
                    // reset scale values
                    SCALEX = float(size.x)/float(GAME_WIDTH);
                    SCALEY = float(size.y)/float(GAME_HEIGHT);
                }
                
                else if( event.key.code == sf::Keyboard::F7 ) { // F7
                    // toggle VSYNC
                    VSYNC ^= 1;
                    recreateWindow(window);                
                    
                }
            }
                
            else if( event.type == sf::Event::Closed )
                window.close();
            
            else if( event.type == sf::Event::Resized ) {
                
                // check screen size:game size ratio
                sf::Vector2u size = window.getSize();
                if( float(size.x)/size.y != float(GAME_WIDTH)/GAME_HEIGHT )
                    texture.setSmooth(true);
                else
                    texture.setSmooth(false);
            }
            
            // call game state events
            game_state -> handle_events(event);
            
        }

        // handle input (keys pressed) independent of event loop
        game_state -> handle_input();

        // update game state
        loops = 0;
        ticks = clock.getTicks();
        while( ticks > lastframe && loops < SKIP_FRAMES_MAX ) {
            
            // update game state here
            update_ticks = clock.getTicks();
            game_state -> update( update_ticks - last_update );
            last_update = update_ticks;
          
            // update misc stuff here
            
            
            // update the fps string
            fps_str = to_string( clock.getFPS() );

            if( show_fps ) {
                
                text.setString( fps_str );
                text.setColor(WHITE);
            
            }

            lastframe += SKIP_FRAMES;
            loops++;
        }
        
        // check for game state change
        if( game_state -> exit ) {
          
            if( game_state -> next == NULL )
                window.close();
          
            else {
                GameState * temp = game_state;
                game_state = temp -> next;
                delete temp;
            }

        }

        // clear screen
        texture.clear();
        window.clear();

        // draw game here
        game_state -> draw(texture);
        texture.display();
        sf::Sprite temp(texture.getTexture());
        temp.scale( SCALEX,SCALEY );
        window.draw( temp );

        // draw misc stuff here
        
        
        // draw the fps string
        if( show_fps )
            window.draw( text );

        // update the window
        window.display();
    }

    return 0;
}
