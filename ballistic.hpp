#ifndef BALLISTIC_HPP
#define BALLISTIC_HPP


#include "gamesprite.hpp"
#include <string>

// forward declaration
class Scene;


class Ballistic : public GameSprite {
    
    public:
    
        Ballistic(std::string,Scene*,sf::Vector2f,int);
        ~Ballistic();
    
        void handle_input();
        void update(int ticks);
        void draw(sf::RenderTexture &window);
        
        bool expired;
    
    
    private:
    
        Scene * parent;
    
        sf::Vector2f position;
        sf::Vector2f orientation;
        sf::Vector2f velocity;
        
        int lifetime;
        int lifeticks;
        
        
    
};

#endif // BALLISTIC_HPP
