#ifndef GAMESTATE_H
#define GAMESTATE_H

//#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

class GameState {
  
  public:
    virtual void handle_input() = 0;
    virtual void handle_events(sf::Event&) = 0;
    virtual void update(int) = 0;
    virtual void draw(sf::RenderTexture&) = 0;

    virtual ~GameState() = 0;

    GameState * next;
    bool exit;
};



#endif