#include <iostream>
#include "locals.hpp"
#include "menu.hpp"
#include "bg_stars.hpp"
#include "scene.hpp"

Menu::Menu( Background * bg_ptr ) : GameState::GameState() {

    //~ if (!font.loadFromFile("cour.ttf")) 
        //~ std::cout<<"Font not found"<<std::endl;
        
    // busy loop to prevent crash in font-loading
    while( !font.loadFromFile("cour.ttf") );
    
    
    text = sf::Text( "Press Enter to Continue", font, 24 );
    text.setStyle( sf::Text::Bold );
    text.setColor( sf::Color(0,0,0) );
    int width         = text.getLocalBounds().width;
    int height        = text.getLocalBounds().height;
    text.setPosition( GAME_WIDTH/2 - width/2, GAME_HEIGHT/2 - height/2 );

    menu_time = 0;
    next = NULL;
    
    bg = bg_ptr;
    
}


Menu::~Menu() {
  
    //if( next != NULL )
    //  delete next;

    if( bg != NULL )
        delete bg;
  
}


void Menu::handle_events(sf::Event & event) {

    if( event.type == sf::Event::KeyPressed &&
              ( event.key.code == sf::Keyboard::Escape ) ) {
        exit = 1;
    }
    else if( event.type == sf::Event::KeyPressed &&
            event.key.code == sf::Keyboard::Return ) {
        exit = 1;
        next = new Scene();
    }

}


void Menu::handle_input( ) {
  
}


void Menu::update(int ticks) {

    menu_time += ticks;

    bg->update(ticks);

}

void Menu::draw(sf::RenderTexture &window) {

    bg->draw( window );  

    window.draw(text);
    text.setColor( sf::Color(255,255,255));
    text.move(-1,-1);
    window.draw(text);
    text.move(1,1);
    text.setColor( sf::Color(0,0,0) );

}
