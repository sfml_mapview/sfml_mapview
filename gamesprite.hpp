#ifndef GAMESPRITE_H
#define GAMESPRITE_H


#include <SFML/Graphics.hpp>
#include <string>



class GameSprite : public sf::Sprite {

  public:
    GameSprite( const std::string &texture_str );
    //~ ~GameSprite();
    
    virtual void handle_input()=0;
    virtual void update(int)=0;
    virtual void draw(sf::RenderTexture&)=0;
    
    bool alive;
    

};






#endif