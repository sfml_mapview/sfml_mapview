#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "gamesprite.hpp"
#include "scene.hpp"
#include <string>


// forward declaration for player constructor
class Scene;


class Player : public GameSprite {
  
    public:
    
        Player( Scene * );
        ~Player();
        
        void handle_input();
        void update(int ticks);
        void draw(sf::RenderTexture &window);
        
        void move( sf::Vector2f & );
        void clearVect();
        
        int getLayer();
        
        bool checkCollision(sf::Vector2f);
    
        void setGamePosition(sf::Vector2f);
        sf::Vector2i getGamePosition();
        sf::Vector2i drawOffset;
    
    private:
    
        Scene * parent;
    
        sf::Vector2f vect;                      // delta movement vector
        sf::Vector2f gamePosition;              // current position
        
        
        int speed;
        int height;
        int width;
        
        int facing;
        int step;
        bool moving;
        int moveTime;
        int cycleTime;
        
        int layer;

};


#endif // PLAYER_H

