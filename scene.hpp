#ifndef SCENE_HPP
#define SCENE_HPP

#include <Thor/Particles.hpp>
#include "gamestate.hpp"
#include "background.hpp"
#include "map.hpp"
#include "player.hpp"

// forward declaration
class Player;

class Scene : public GameState {
  
    public:
    
        Scene();
        ~Scene();

        void handle_input();
        void handle_events(sf::Event &);
        void update(int);
        void draw(sf::RenderTexture&);
        void pause();
        
        void printCollisionMap(bool**);
        bool ** getCollisionMap(int);
        
        void renderScene();

    private:
    
        sf::RenderTexture current;
        std::vector<bool**> collisionMaps;
        std::vector<Tmx::ObjectGroup*> mapObjectGroups;

        int scene_time;
        float scrollSpeed;
        
        bool paused;
        Background * bg;
        Map * map;
        Player * player;
        
        sf::Font font;
        sf::Text statusText;
        sf::Text pauseText;
        
};

#endif
