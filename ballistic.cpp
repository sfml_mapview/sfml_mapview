#include "ballistic.hpp"




Ballistic::Ballistic(   std::string sprite,Scene* p,sf::Vector2f pos,int lftm  ) 
                        : GameSprite(sprite) {

    parent = p;
    position = pos;
    lifetime = lftm;
    
    expired = false;
    lifeticks = 0;

}



Ballistic::~Ballistic() {
}

void Ballistic::handle_input() {
}




void Ballistic::update(int ticks) {
    
    lifeticks += ticks;
    
    if( lifeticks >= lifetime )
        expired = true;

}




void Ballistic::draw(sf::RenderTexture &window) {
}


