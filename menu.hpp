#include "gamestate.hpp"
#include "background.hpp"
#include "bg_solid.hpp"
//#include "bg_stars.hpp"
//#include "bg_scroll.hpp"

#ifndef MENU_H
#define MENU_H

class Menu : public GameState {

    public:
        Menu( Background * bg_ptr = new BgSolid() );
        ~Menu();
        void handle_input();
        void handle_events(sf::Event &);
        void update(int);
        void draw(sf::RenderTexture&);

    private:
        int menu_time;
        Background * bg;
        
        sf::Font font;
        sf::Text text;

};




#endif
