#ifndef BG_SCROLL_H
#define BG_SCROLL_H

//#include <SFML/Graphics.hpp>
#include <vector>
#include "background.hpp"
#include "locals.hpp"


struct Layer {
  
    Layer();
    Layer( sf::Texture * txt, int dir, int xpos, int ypos, int spd );

    sf::Sprite img;
    sf::IntRect rect;

    float shift_pos;

    int xPos;
    int yPos;
    int speed;
    int height;
    int width;
    int direction;

    void shift(const int&);
  
};



class BgScroll : public Background {
  
    public:
        BgScroll();
        BgScroll( const std::vector<Layer>&, float spd=1.0f );
        //~BgScroll();                          // no need?
        void update(int);
        void draw(sf::RenderWindow&);
    
    private:
        sf::RenderTexture background;
        sf::Sprite bg_sprite;
        std::vector<Layer> layers;
        float speed;
        int bg_time;

};


#endif
